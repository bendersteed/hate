# hate

A simple web app for voting on your most hated personalities.

## Installation

Just clone and run lein run. 

## Running

You need to start a postgresql database. This can be done by running 

``` shell
pg_ctl -D pg -l logfile start 
createdb hate
```

The role will be your username, so use that in db-spec in core.clj.

Then you can play with the API through
swagger at http://localhost:3000/swagger

``` shell
lein run #runs the backend
lein fig #builds the frontend and starts a figwheel repl
```

## License

Copyright © 2019 bendersteed

This program and the accompanying materials are made available under the
terms of the GNU Public License 3.0 which is available at
https://www.gnu.org/licenses/gpl-3.0.html.
