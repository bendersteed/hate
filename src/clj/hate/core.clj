(ns hate.core
  (:require [toucan.db :as db]
            [toucan.models :as models]
            [ring.adapter.jetty :refer [run-jetty]]
            [compojure.api.sweet :refer [routes api]]
            [hate.persona :refer [persona-api]]
            [compojure.route :as route])
  (:gen-class))

(def db-spec
  {:dbtype "postgres"
   :dbname "hate"
   :user "bendersteed"
   :password ""})

(def swagger-config
  {:ui "/swagger"
   :spec "/swagger.json"
    :options {:ui {:validatorUrl nil}
              :data {:info {:version "1.0.0", :title "Restful CRUD API"}}}})

(def app (routes
          (api {:swagger swagger-config} (apply routes persona-api))
          (route/resources "/")
          (route/not-found "404 There is nothing here")))
(defn -main
  [& args]
  (db/set-default-db-connection! db-spec)
  (models/set-root-namespace! 'hate.model)
  (run-jetty app {:port 3000}))
