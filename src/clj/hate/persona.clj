(ns hate.persona
  (:require [schema.core :as s]
            [hate.string-util :as utils]
            [hate.model.persona :refer [Persona]]
            [toucan.db :as db]
            [ring.util.http-response :refer [created ok not-found
                                             file-response internal-server-error]]
            [compojure.api.sweet :refer [POST GET]]
            [cheshire.core :refer [generate-string]]))

(defn valid-name? [name]
  (and (utils/non-blank? name)
       (utils/lenght-in-range? 5 50 name)))

(s/defschema PersonaRequestSchema
  {:name (s/constrained s/Str valid-name?)
   :votes (s/constrained s/Int int?)})

(defn personas->json []
  (->> (db/select Persona)
       generate-string))

(defn id->created [id]
  (created (str "personas/" id) (personas->json)))

(defn votes-inc-response! [persona-request]
  (let [row (first (db/select Persona :name (:name persona-request)))
        id (:id row)
        votes (:votes row)]
    (if (db/update! Persona id :votes (inc votes))
      (created (str "personas/" id) (personas->json))
      (internal-server-error))))

(defn create-persona-handler [create-persona-req]
  (if (db/exists? Persona :name (:name create-persona-req))
    (votes-inc-response! create-persona-req)
    (->> (db/insert! Persona create-persona-req)
        :id
        id->created)))

(defn persona->response [persona]
  (if persona
    (ok (generate-string persona))
    (not-found)))

(defn get-persona-handler [persona-id]
  (-> (Persona persona-id)
      persona->response))

(defn get-personas-handler []
  (-> (personas->json)
      (ok)))

(def persona-api
  [(POST "/personas" []
         :body [create-persona-req PersonaRequestSchema]
         (create-persona-handler create-persona-req))
   (GET "/personas/:id" []
        :path-params [id :- s/Int]
        (get-persona-handler id))
   (GET "/personas" []
        (get-personas-handler))])
