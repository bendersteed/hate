(ns hate.string-util
  (:require [clojure.string :as str]))

(def non-blank? (complement str/blank?))

(defn max-length? [length text]
  (<= (count text) length))

(defn min-length? [length text]
  (>= (count text) length))

(defn lenght-in-range? [min-length max-length text]
  (and (min-length? min-length text)
       (max-length? max-length text)))
