(ns hate.core
  (:require [reagent.core :as r]
            [ajax.core :refer [GET PUT POST]]
            [clojure.string :refer [split capitalize join]]))

;; state
(def personas (r/atom []))
(def quote (r/atom []))

;; api-consuming functions
(defn handler! [response]
  (reset! personas response)
  (swap! personas (partial sort-by :votes >)))

(defn quote-handler! [response]
  (reset! quote response))

(defn get-personas! []
  (GET "/personas" {:response-format :json
                    :keywords? true
                    :handler handler!}))

(defn post-personas! [val]
  (let [name (join " " (map capitalize (split val " ")))]
   (POST "/personas" {:format :json
                      :params {:name name
                               :votes 1}
                      :handler handler!
                      :response-format :json
                      :keywords? true})))

(defn get-quote! []
  (GET "https://thesimpsonsquoteapi.glitch.me/quotes"
       {:response-format :json
        :keywords? true
        :handler quote-handler!}))

;; data management
(defn sort-personas [title sort-key fn]
  (when-not (empty? @personas)
    [:input.sort {:type "button"
             :on-click #(swap! personas (partial sort-by sort-key fn))
                  :value (str "sort posts by " title)}]))

(defn toggle-class [a class1 class2]
  (if (= @a class1)
    (reset! a class2)
    (reset! a class1)))

;; components
(defn header []
  [:div.header [:pre "
 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          
▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌
 ▀         ▀  ▀         ▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀ 
"]
   [:h3 "Sometimes hate consumes you, sometimes you need to vent off."]])

(defn separator []
  [:div.separator "====================================================================="])


(defn persona-input []
  (let [val (r/atom "")]
    (fn []
      [:div.persona-input
       [:input {:class "form-control"
                :type "text"
                :value @val
                :placeholder "Enter the target of your wrath"
                :pattern ".{5,50}"
                :on-change #(reset! val (-> % .-target .-value))}]
       [:input {:type "button"
                :value "submit"
                :on-click #(post-personas! @val)}]])))

(defn personas-table []
  (let [class-ten (r/atom "show")
        class-all (r/atom "hide")]
    (do (get-personas!)
              (fn []
                [:div.persona-table 
                 [:div {:class @class-ten} [:h3 "10 most hated"]
                  [:ul (take 10 (for [x @personas]
                                  [:li {:key (:name x)}
                                   (str (:name x)
                                        " is hated by " (:votes x))]))]
                  [:input {:type "button"
                           :value "show all"
                           :on-click
                           #(do (toggle-class class-ten "hide" "show")
                                (toggle-class class-all "hide" "show"))}]]
                 [:div {:class @class-all}
                  [:h3 "All hated by you and me!"]
                  [sort-personas "votes" :votes >]
                  [sort-personas "name" :name <]
                  [:ul (for [x @personas]
                         [:li {:key (:name x)}
                          (str (:name x)
                               " is hated by " (:votes x))])]
                  [:input {:type "button"
                           :value "show just 10"
                           :on-click
                           #(do (toggle-class class-ten "hide" "show")
                                (toggle-class class-all "hide" "show"))}]]]))))

(defn quote-div []
  (do (get-quote!)
      (fn []
        [:div.quote
         [:h3 "Afterwards it's good to have some fun!"]
         [:img {:src (:image (first @quote))}]
         [:h3 (:quote (first @quote))]
         [:span (:character (first @quote))]])))

(defn footer []
  [:footer.footer
   [:h3 "This site would not be possible without the wonderful work of:"]
   [:ul [:li "Clojure/Ring/Compojure-api/Clojurescript/Reagent/ClJS-Ajax 
-- they are the main libraries used"]
    [:li "Schema/Toucan/Postgresql for databases"]
    [:li [:a {:href "https://thesimpsonsquoteapi.glitch.me/"}
          "The Simpsons' quote API"]
     " for the Simpsons quotes"]]
   [:pre "
┌┐ ┌─┐┌┐┌┌┬┐┌─┐┬─┐┌─┐┌┬┐┌─┐┌─┐┌┬┐          ┌┬┐┬ ┬┬┌┐┌┬┌─  ┌─┐┌─┐┬─┐  ┬ ┬┌─┐┬ ┬┌─┐┌─┐┬  ┌─┐   
├┴┐├┤ │││ ││├┤ ├┬┘└─┐ │ ├┤ ├┤  ││  ──────   │ ├─┤││││├┴┐  ├┤ │ │├┬┘  └┬┘│ ││ │└─┐├┤ │  ├┤    
└─┘└─┘┘└┘─┴┘└─┘┴└─└─┘ ┴ └─┘└─┘─┴┘           ┴ ┴ ┴┴┘└┘┴ ┴  └  └─┘┴└─   ┴ └─┘└─┘└─┘└─┘┴─┘└  ┘  
┌─┐┌─┐┬ ┬┌┬┐┬ ┬┌─┐┬┌─┬
└─┐│  ├─┤││││ ││  ├┴┐│
└─┘└─┘┴ ┴┴ ┴└─┘└─┘┴ ┴o
"]])

(defn home-page []
  [:div [header]
   [persona-input]
   [personas-table]
   [separator]
   [quote-div]
   [separator]
   [footer]])

(defn run []
  (r/render [home-page]
            (js/document.getElementById "app")))

(run)
