(defproject hate "0.1.0"
  :description "A webapp for channeling your inner rage"
  :url "http://hate.me"
  :license {:name "GPL-3.0"
            :url "https://www.gnu.org/licenses/gpl-3.0.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.339"]
                 [prismatic/schema "1.1.9"]
                 [metosin/compojure-api "2.0.0-alpha30"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [compojure "1.6.1"]
                 [toucan "1.1.9"]
                 [org.postgresql/postgresql "42.2.4"]
                 [reagent "0.8.1"]
                 [cljs-ajax "0.8.0"]
                 [cheshire "5.8.1"]]
  :source-paths ["src/clj" "src/cljs"]
  :resource-paths ["target" "resources"]
  :main ^:skip-aot hate.core
  :uberjar-name ["hate-0.1.0-standalone.jar"]
  :target-path "target/%s"
  :aliases {"fig" ["trampoline" "run" "-m" "figwheel.main"]
            "build-dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]}
  :profiles {:uberjar {:aot :all}
             :dev
             {:dependencies [[cider/piggieback "0.4.1"]
                             [com.bhauman/figwheel-main "0.2.0"]
                             [com.bhauman/rebel-readline-cljs "0.1.4"]
                             [figwheel-sidecar "0.5.16"]]
              :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]}}})
